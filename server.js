//server.js
var express = require('express');
var app = express();
var routes = require('./routes');
var bodyParser = require("body-parser");
var epx = require('')
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(express.static(__dirname + '/public'));

app.get('/:template',routes.template());
app.post('/logIn',routes.logIn());

app.listen(8080,function(){
    console.log("Server running on port 8080");
});
//