//public/js/controllers
var authApp = angular
                .module('app')
                .controller('authCtrl',authCtrl);

authApp.$injector = ['$scope','$http','authService'];

function authCtrl($scope,$location,$http,authService){
    $scope.logIn = function(username, password){
        if (username !== undefined && password !== undefined) {

           authService.logIn(username,password).success(function(data){
               ////authService.isLogged = true;
               //console.log(data)
               if(data.access === 'true'){
                   $location.path("/loginFailed");
               }else {
                   $location.path("/loginFailed");
               }

           }).error(function(data,status){
               console.log(data);
               console.log(status);
           });
        };

    };
    $scope.logOut = function(){
        if(authService.isLogged){
            authService.isLogged = false;
            delete $window.sessionStorage.token;
            $location.path('#/auth');
        }
    }
}
