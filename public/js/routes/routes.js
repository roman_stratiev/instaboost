// ./public/js/routes/routes.js
var routes = angular.module('app');

routes.$injector=[];
routes.config(['$routeProvider',function($routeProvider){
        $routeProvider
            .when('/auth',{
                templateUrl:'../views/auth.html'
            })
            .when('/reg',{
                templateUrl:'../views/register.html'
            })
            .when('/account/me',{
                templateUrl:'../views/account/me.html'
            })
            .when('/account/task',{
                templateUrl:'../views/account/task.html'
            })
            .when('/account/accounts',{
                templateUrl:'../views/account/accounts.html',
            })
            .when('/account/cabinet',{
                templateUrl:'../views/account/cabinet.html'
            })
            .when('/loginFailed',{
                templateUrl:'../views/loginFailed.html'
            })

}]);
